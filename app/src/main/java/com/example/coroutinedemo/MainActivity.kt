package com.example.coroutinedemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private var count = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                introduceThread()
            }
        }
//        button2.setOnClickListener {
//            CoroutineScope(Dispatchers.Main).launch {
//                introduceThread()
//            }
//        }

    }

    private fun introduceThread() {
        textView.text = "Hello from thread ${Thread.currentThread().name}"
    }
}
